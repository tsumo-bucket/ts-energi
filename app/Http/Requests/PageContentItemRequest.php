<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PageContentItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content_id' => 'integer',

            'title' => 'max:255',

            'description' => '',

            'image' => 'integer|exists:assets,id',

            'bg_image' => 'integer',

            'type' => 'in:default,image,input,file upload',

            'published' => 'in:draft,published',

            'order' => 'integer',

            'name' => 'max:255',

            'slug' => '',

            'enabled' => '',

            'editable' => '',
        ];
    }
}
