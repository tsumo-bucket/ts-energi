{$INPUTS}<div class="form-group clearfix">
	<a href="{{route('admin{$ROUTE}')}}" class="btn btn-default">Back</a>
	<button type="submit" class="btn btn-primary float-right">
		<i class="fa fa-check" aria-hidden="true"></i>
		Save
	</button>
</div>